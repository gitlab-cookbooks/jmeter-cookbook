#
# Cookbook: jmeter
# License: Apache 2.0
#
# Copyright 2014-2015, Bloomberg Finance L.P.
#

default['jmeter']['install_type'] = 'source'

default['jmeter']['version'] = '5.4'
default['jmeter']['source_url'] = 'https://downloads.apache.org//jmeter/binaries/apache-jmeter-5.4.tgz'
default['jmeter']['source_checksum'] = '2a900f56f106af5f165f408b61988a5fbde2b3c6dc5e7e9ccedbed0c01dc7ec1'

default['jmeter']['package_name'] = 'jmeter'

default['nokogiri']['version'] = '1.6.7.1'
